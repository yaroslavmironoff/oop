"""

Функция issubclass()
Наследование классов от класса object
Наследование классов от встроенных типов

"""


class Geom:
    pass


class Line(Geom):
    pass


class Vector(list):
    def __str__(self):
        return " ".join(map(str, self))


g = Geom()
li = Line()
print(issubclass(Line, Geom))
print(isinstance(li, Geom))
print(isinstance(li, object))
print(issubclass(Geom, Line))
print(issubclass(int, object))      # Встроенные типы - классы
print(issubclass(list, object))     # Встроенные типы - классы

v = Vector([1, 2, 3])
print(v)
