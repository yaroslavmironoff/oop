# Магические методы __str__, __repr__, __len__, __abs__.

# класс Cat смотреть в Python Console
class Cat:
    def __init__(self, name):
        self.name = name

    #отображение информации об объекте класса в режиме отладки(для разработчика)
    def __repr__(self):
        return f"{self.__class__}: {self.name}"

    #отображение информации об объекте класса для пользователя(например, функции print, str)
    def __str__(self):
        return f"{self.name}"


class Point:
    def __init__(self, *args):
        self.__coords = args

    # позволяет применять функцию len() к экземплярам класса
    def __len__(self):
        return len(self.__coords)

    # позволяе применять функцию abs() к экземплярам класса
    def __abs__(self):
        return list(map(abs, self.__coords))

p = Point(1, -3, -5)
print(len(p))
print(abs(p))
