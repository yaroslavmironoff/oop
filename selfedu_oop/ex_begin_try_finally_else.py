"""

Введение в обработку исключений. Блоки try/except. Блоки finally и else.

"""


def div(a, b):
    try:
        return a / b
    except ZeroDivisionError as z:
        return print(z)


res = 0
try:
    x, y = map(int, input().split())
    res = div(x, y)
    # with open("myfile.txt") as f:
    #     f.write("hello")
    # x, y = map(int, input().split())
    # res = x / y
except ValueError as z:
    print(z)

print(res)
