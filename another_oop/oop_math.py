"""

Напишите программу с классом Math. Создайте два атрибута — a и b. Напишите методы addition —
сложение, multiplication — умножение, division — деление, subtraction — вычитание. При передаче
в методы параметров a и b с ними нужно производить соответствующие действия и печатать ответ.

"""


class Maths:

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def addition(self):
        sum_res = self.a + self.b
        return sum_res

    def multiplication(self):
        mul_res = self.a * self.b
        return mul_res

    def division(self):
        try:
            div_res = self.a / self.b
            return div_res
        except ZeroDivisionError:
            print("Ошибка: деление на ноль")

    def subtraction(self):
        sub_res = self.a - self.b
        return sub_res


Calc = Maths(2, 5)
print(f"Сложение: {Calc.addition()}")
print(f"Вычитание: {Calc.subtraction()}")
print(f"Умножение: {Calc.multiplication()}")
print(f"Деление: {Calc.division()}")
