"""

Напишите программу с классом Car. Создайте конструктор класса Car. Создайте атрибуты класса Car —
color (цвет), type (тип), year (год). Напишите пять методов. Первый — запуск автомобиля, при его
вызове выводится сообщение «Автомобиль заведен». Второй — отключение автомобиля — выводит сообщение
«Автомобиль заглушен». Третий — присвоение автомобилю года выпуска. Четвертый метод — присвоение
автомобилю типа. Пятый — присвоение автомобилю цвета.

"""


class Car:

    def __init__(self, color=0, types=0, year=0):
        self.color = color
        self.types = types
        self.year = year

    def launch(self):
        print("Запуск автомобиля")

    def cancel(self):
        print("Автомобиль заглушен")

    def set_year(self, year_s):
        self.year = year_s

    def set_type(self, type_s):
        self.types = type_s

    def set_color(self, color_s):
        self.color = color_s


car = Car()
car.set_color("Red")
car.set_type("Van")
car.set_year(2010)
car.launch()
car.cancel()
