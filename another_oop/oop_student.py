"""

Напишите программу с классом Student, в котором есть три атрибута: name, groupNumber и age.
По умолчанию name = Ivan, age = 18, groupNumber = 10A. Необходимо создать пять методов: getName,
getAge, getGroupNumber, setNameAge, setGroupNumber. Метод getName нужен для получения данных об
имени конкретного студента, метод getAge нужен для получения данных о возрасте конкретного
студента, метод setGroupNumber нужен для получения данных о номере группы конкретного студента.
Метод SetNameAge позволяет изменить данные атрибутов установленных по умолчанию, метод
setGroupNumber позволяет изменить номер группы установленный по умолчанию. В программе
необходимо создать пять экземпляров класса Student, установить им разные имена, возраст и номер
группы.

"""


class Student:

    def __init__(self, name="Ivan", age=18, group_number="10A"):
        self.name = name
        self.age = age
        self.group_number = group_number

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def get_group_number(self):
        return self.group_number

    def set_name_age(self, name_s, age_s):
        self.name = name_s
        self.age = age_s

    def set_group_number(self, group_num):
        self.group_number = group_num


Ivan = Student()
print(f"Имя: {Ivan.get_name()}, Возраст: {Ivan.get_age()}, Номер группы: {Ivan.get_group_number()}")

John = Student()
John.set_name_age("John", 19)
John.set_group_number("10B")
print(f"Имя: {John.get_name()}, Возраст: {John.get_age()}, Номер группы: {John.get_group_number()}")

Petr = Student()
Petr.set_name_age("Petr", 17)
Petr.set_group_number("10C")
print(f"Имя: {Petr.get_name()}, Возраст: {Petr.get_age()}, Номер группы: {Petr.get_group_number()}")

Nick = Student()
Nick.set_name_age("Nick", 18)
Nick.set_group_number("10F")
print(f"Имя: {Nick.get_name()}, Возраст: {Nick.get_age()}, Номер группы: {Nick.get_group_number()}")

Kanye = Student()
Kanye.set_name_age("Kanye", 20)
Kanye.set_group_number("10G")
print(f"Имя: {Kanye.get_name()}, Возраст: {Kanye.get_age()}, Номер группы: {Kanye.get_group_number()}")

Loyd = Student()
Loyd.set_name_age("Loyd", 21)
Loyd.set_group_number("10K")
print(f"Имя: {Loyd.get_name()}, Возраст: {Kanye.get_age()}, Номер группы: {Kanye.get_group_number()}")
