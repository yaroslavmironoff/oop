"""

Изменить класс Topology из задания 22.1a или 22.1.

Добавить метод delete_link, который удаляет указанное соединение. Метод должен удалять и «обратное» соединение, если оно есть (ниже пример).

Если такого соединения нет, выводится сообщение «Такого соединения нет».

Создание топологии

In [7]: t = Topology(topology_example)

In [8]: t.topology
Out[8]:
{('R1', 'Eth0/0'): ('SW1', 'Eth0/1'),
 ('R2', 'Eth0/0'): ('SW1', 'Eth0/2'),
 ('R2', 'Eth0/1'): ('SW2', 'Eth0/11'),
 ('R3', 'Eth0/0'): ('SW1', 'Eth0/3'),
 ('R3', 'Eth0/1'): ('R4', 'Eth0/0'),
 ('R3', 'Eth0/2'): ('R5', 'Eth0/0')}
Удаление линка:

In [9]: t.delete_link(('R3', 'Eth0/1'), ('R4', 'Eth0/0'))

In [10]: t.topology
Out[10]:
{('R1', 'Eth0/0'): ('SW1', 'Eth0/1'),
 ('R2', 'Eth0/0'): ('SW1', 'Eth0/2'),
 ('R2', 'Eth0/1'): ('SW2', 'Eth0/11'),
 ('R3', 'Eth0/0'): ('SW1', 'Eth0/3'),
 ('R3', 'Eth0/2'): ('R5', 'Eth0/0')}
Удаление «обратного» линка: в словаре есть запись ('R3', 'Eth0/2'): ('R5', 'Eth0/0'), но вызов delete_link с указанием ключа и значения в обратном порядке, должно удалять соединение:

In [11]: t.delete_link(('R5', 'Eth0/0'), ('R3', 'Eth0/2'))

In [12]: t.topology
Out[12]:
{('R1', 'Eth0/0'): ('SW1', 'Eth0/1'),
 ('R2', 'Eth0/0'): ('SW1', 'Eth0/2'),
 ('R2', 'Eth0/1'): ('SW2', 'Eth0/11'),
 ('R3', 'Eth0/0'): ('SW1', 'Eth0/3')}
Если такого соединения нет, выводится сообщение:

In [13]: t.delete_link(('R5', 'Eth0/0'), ('R3', 'Eth0/2'))
Такого соединения нет

"""

topology_example = {('R1', 'Eth0/0'): ('SW1', 'Eth0/1'),
                    ('R2', 'Eth0/0'): ('SW1', 'Eth0/2'),
                    ('R2', 'Eth0/1'): ('SW2', 'Eth0/11'),
                    ('R3', 'Eth0/0'): ('SW1', 'Eth0/3'),
                    ('R3', 'Eth0/1'): ('R4', 'Eth0/0'),
                    ('R3', 'Eth0/2'): ('R5', 'Eth0/0'),
                    ('SW1', 'Eth0/1'): ('R1', 'Eth0/0'),
                    ('SW1', 'Eth0/2'): ('R2', 'Eth0/0'),
                    ('SW1', 'Eth0/3'): ('R3', 'Eth0/0')}


class Topology:
    def __init__(self, topology_net):
        self.topology = {}
        for key, value in topology_net.items():
            if not self.topology.get(value) == key:
                self.topology[key] = value

    def get(self):
        # вывод словаря без дублирующих линков
        print(f"\ntopology:")
        for key, value in self.topology.items():
            print(f"{key}{value}")


    def delete_link(self, del_topology_key, del_topology_value):
            if self.topology.get(del_topology_key) == del_topology_value:
                del self.topology[del_topology_key]
            elif self.topology.get(del_topology_value) == del_topology_key:
                del self.topology[del_topology_value]
            else:
                print(f"\nТакого соединения нет")


# создание экземпляра класса
top = Topology(topology_example)
# вывод переменной экземпляра класса
print(f"Переменная topology: {top.topology}")
# вывод результирующего словаря в столбик
top.get()
# удаление линка
top.delete_link(('R3', 'Eth0/1'), ('R4', 'Eth0/0'))
top.get()
# удаление "обратного" линка
top.delete_link(('R5', 'Eth0/0'), ('R3', 'Eth0/2'))
top.get()
# вывод сообщения, что такого соединения нет
top.delete_link(('R5', 'Eth0/0'), ('R3', 'Eth0/2'))