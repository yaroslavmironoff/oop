"""

Скопировать класс Topology из задания 22.1 и изменить его.

Перенести функциональность удаления «дублей» в метод _normalize. При этом метод __init__ должен выглядеть таким образом:

class Topology:
    def __init__(self, topology_dict):
        self.topology = self._normalize(topology_dict)

"""

topology_example = {('R1', 'Eth0/0'): ('SW1', 'Eth0/1'),
                    ('R2', 'Eth0/0'): ('SW1', 'Eth0/2'),
                    ('R2', 'Eth0/1'): ('SW2', 'Eth0/11'),
                    ('R3', 'Eth0/0'): ('SW1', 'Eth0/3'),
                    ('R3', 'Eth0/1'): ('R4', 'Eth0/0'),
                    ('R3', 'Eth0/2'): ('R5', 'Eth0/0'),
                    ('SW1', 'Eth0/1'): ('R1', 'Eth0/0'),
                    ('SW1', 'Eth0/2'): ('R2', 'Eth0/0'),
                    ('SW1', 'Eth0/3'): ('R3', 'Eth0/0')}


class Topology:
    def __init__(self, topology_dict):
        self.topology = self._normalize(topology_dict)

    def _normalize(self, topology_norm):
        self.topology = {}
        for key, value in topology_norm.items():
            if not self.topology.get(value) == key:
                self.topology[key] = value
        return self.topology

    def get(self):
        # вывод словаря без дублирующих линков
        print("topology:")
        for key, value in self.topology.items():
            print(f"{key}{value}")


# создание экземпляра класса
top = Topology(topology_example)
# вывод переменной экземпляра класса
print(f"Переменная topology: {top.topology}")
# вывод результирующего словаря в столбик
top.get()