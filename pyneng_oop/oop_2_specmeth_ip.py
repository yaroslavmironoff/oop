"""

В этом задании необходимо создать класс IPAddress.

При создании экземпляра класса, как аргумент передается IP-адрес и маска, а также должна выполняться
проверка корректности адреса и маски:

Адрес считается корректно заданным, если он:

состоит из 4 чисел разделенных точкой
каждое число в диапазоне от 0 до 255
Маска считается корректной, если это число в диапазоне от 8 до 32 включительно

Если маска или адрес не прошли проверку, необходимо сгенерировать исключение ValueError с соответствующим
текстом (вывод ниже).

Также, при создании класса, должны быть созданы две переменных экземпляра: ip и mask, в которых содержатся
адрес и маска, соответственно.

Пример создания экземпляра класса:

In [1]: ip = IPAddress('10.1.1.1/24')

Атрибуты ip и mask
In [2]: ip1 = IPAddress('10.1.1.1/24')

In [3]: ip1.ip
Out[3]: '10.1.1.1'

In [4]: ip1.mask
Out[4]: 24
Проверка корректности адреса (traceback сокращен)

Проверка корректности маски (traceback сокращен)

In [6]: ip1 = IPAddress('10.1.1.1/240')
---------------------------------------------------------------------------
...
ValueError: Incorrect mask

"""


class IPAddress:
    def __init__(self, ipaddress):
        ip, mask = ipaddress.split('/')
        self._check_ip(ip)
        self._check_mask(mask)
        self.ip = ip
        self.mask = int(mask)

    def _check_ip(self, ip):
        check_ip = ip.split('.')
        if len(check_ip) != 4:
            raise ValueError("Incorrect IPv4")
        for i in range(len(check_ip)):
            if not check_ip[i].isdigit():
                raise ValueError("Incorrect IPv4")
            if int(check_ip[i]) < 0 or int(check_ip[i]) > 255:
                raise ValueError("Incorrect IPv4")
        return True

    def _check_mask(self, mask):
        if not mask.isdigit():
            raise ValueError("Incorrect mask")
        if int(mask) < 8 or int(mask) > 32:
            raise ValueError("Incorrect mask")
        return True

#Пример правильного адреса
ip = IPAddress('10.1.1.1/28')
print(ip.ip)
print(ip.mask)
#Пример некорректного адреса
ip1 = IPAddress('1000.1.1.1/280')
print(ip1.ip)
print(ip1.mask)




